#include <stdio.h>
#include <stdlib.h>
#include <regex.h>
#include <string.h>
#include <sys/types.h>
#include <assert.h>

void list();
char *reg (FILE *filename);
void searchfile(char *name);

int main(){

  int i, reti;
  float E;
  char *c, *errmsg, *match;
  FILE *fp2, *fp3;
  regex_t regex;
  
  /*  void list(){
    //  c = 'A Char holding a string.';
    c = "A String.";
    printf("%c\n", c[3]);
    system("ls -l > ls.out");
    }
  */
  fp3 = fopen("plot.dat", "w");
  fp2 = fopen("ls.out", "r");
  list();
  match = reg(fp2);

  /*  reti = regcomp(&regex, "^ecut[_]*[0-9]+[.]+out$", 0);
  if(reti){
    fprintf(stderr, "Couldnot compile regex.\n");
    exit(1);
  }
  reti = regexec(&regex, "ecut", 0, NULL, 0);
  if(!reti){
    puts("Match.");
  }
  else if(reti == REG_NOMATCH){
    puts("No Match.");
  }
  else{
    regerror(reti, &regex, errmsg, sizeof(errmsg));
    fprintf(stderr, "Regex match failed.\n %s", errmsg);
    exit(1);

    }*/
  //regfree(&regex);
 
  fclose(fp2);
  fclose(fp3);
  return(0);

}


void list(){

  int i, reti;
  float E;
  char *c, *errmsg;
  regex_t regex;
  
  //  c = 'A Char holding a string.';
  c = "A String.";
  printf("%c\n", c[3]);
  system("ls -l > ls.out");
}

char *reg(FILE *filename){

  FILE *fp6; 
  char *line, errmsg[1000], line2[1000], last, *token, *fname;
  const char *delim;
  regex_t regex;
  ssize_t num;
  size_t len = 0;
  int reti, reti2, i, reti3, j;
  regmatch_t re;
  
  //"^ecut_*[0-9]+\\.+out$
  //reti = regcomp(&regex, "^ecut[_]*[0-9]+[.]+out$", 0);
  //reti = regcomp(&regex, "^ecut_*[0-9]+\\.+out$", 0);
  reti = regcomp(&regex, "ecut_*[0-9]+\\.+out", REG_ICASE | REG_EXTENDED);
  if(reti){
      fprintf(stderr, "Couldnot compile regex.\n");
      regerror(reti, &regex, errmsg, sizeof(errmsg));
      fprintf(stderr, "Regex compilation failed.\n %s", errmsg);
      exit(1);
   }
  line = (char*)malloc(100*sizeof(char));
  token = (char*)malloc(100*sizeof(char));
  fname = (char*)malloc(100*sizeof(char));
  len = sizeof(line);
  i = 0;
  delim = "     ";
  // while(fgets(line2, sizeof(line2), filename) != NULL){
  while((num = getline(&line, &len, filename)) != -1){

    //num = getline(&line, &len, filename);
    //last = line[sizeof line/sizeof *line - 1];
    //printf("line:  %s \n", line);
    i++;
    token = strtok(line, delim);
    //printf("First word:  %s\n", token);
    while(token != NULL){
      
      //reti2 = regexec(&regex, token, 0, NULL, 0);
      reti2 = regexec(&regex, token, 0, &re, 0);
      //printf("Reti2:  %d\n", reti2);
      regerror(reti2, &regex, errmsg, sizeof(errmsg));
      //fprintf(stderr, "Regerror status:  %s\n", errmsg);
      //printf("Regerror status:  %s\n", errmsg);
      //fprintf(stderr, "After regexec. \n");
      //printf("Words: %s\n", token);
      //token = strtok(NULL, delim);
      //printf("Words: %s\n", token);
      if(!reti2){
        printf("Match.  %d  Word:%s\n", reti2, token);
	printf("Size of token: %d\n", sizeof(token));
	printf("Null terminated?  %c\n", token[sizeof(token)+2]);
	for(j = 0; j <= sizeof(token)+2; j++){
	  fname[j] = token[j];
	  printf(" %c,      %c\n", *token++, *fname++);
	}
	//while(*token != "/0"){
	//printf(" %c \n", *token++);
	//}
	printf("Size of fname:   %d\n", sizeof(fname));
	printf("fname:  %s\n", fname);
	searchfile(fname);
	printf("After call to searchfile\n");
      }
      //else if(reti2 == REG_NOMATCH)
      
      else if(reti2 == REG_NOMATCH){
        //printf("No Match. %d Word:   %s\n", reti2, token);
	//printf("REG_NOMATCH: %d \n", REG_NOMATCH);
	;
      }
      else{
	
        regerror(reti, &regex, errmsg, sizeof(errmsg));
        fprintf(stderr, "Regex match failed.\n %s", errmsg);
        exit(1);

      }
      token = strtok(NULL, delim);
    }
  }
  regfree(&regex);
  free(line);
  free(token);
  free(fname);
  //free(errmsg);
  printf("Number of lines read : %d \n", i);

}


void searchfile(char *name){

  FILE *fp5;
  regex_t regex;
  regmatch_t re;
  int reti, reti2;
  size_t len=0;
  ssize_t num;
  const char *delim;
  char *token, *line, *errmsg, *line1;

  printf("Inside searchfile\n");
  reti = regcomp(&regex, "\s*lattice\s*", REG_ICASE | REG_EXTENDED);
  //reti = regcomp(&regex, "\s*!\s*", REG_ICASE | REG_EXTENDED);
  if(reti){
      fprintf(stderr, "Couldnot compile regex.\n");
      regerror(reti, &regex, errmsg, sizeof(errmsg));
      fprintf(stderr, "Regex compilation failed.\n %s", errmsg);
      exit(1);
   }
  //printf("reti: %d\n", reti);
  //delim = (char*)malloc(100*sizeof(char));
  errmsg = (char*)malloc(1000*sizeof(char));
  line = (char*)malloc(100000*sizeof(char));
  line1 = (char*)malloc(100000*sizeof(char));
  len = sizeof(line);
  delim = "         ";
  printf("after malloc\n");
  printf("Name of file: %s\n", name);
  fp5 = fopen(name, "r");
  if(fp5 == NULL){
    printf("File open error, %d\n", fp5);
  }
  rewind(fp5);
  printf("After fopen, name: %s\n", name);
  while((num = getline(&line, &len, fp5)) != -1){

    printf("Line:  %s\n", line1);
    strncpy(line1, line, len);
    //printf("Line:  %s\n", line1);
    token = strtok(line, delim);
    while(token != NULL){
      
      //printf("inside 2nd while\n");
      reti2 = regexec(&regex, token, 0, &re, 0);
      //printf("reti2:   %d\n", reti2);
      regerror(reti2, &regex, errmsg, sizeof(errmsg));
      //printf("Regerror status:  %s\n", errmsg);
      //fprintf(stderr, "After regexec. \n");
      //printf("Words: %s\n", token);
      //token = strtok(NULL, delim);
      //printf("reti2: %d\n", reti2);
      if(!reti2){
        //printf("Match.  %d  Word:    %s  Line:    %s  Num:     %d\n", reti2, token, line1, num);
	printf("Line:    %s\n",line1);
      }
      
      else if(reti2 == REG_NOMATCH){
        //printf("No Match. %d Word:   %s\n", reti2, token);
	;
      }
      else{
	
        regerror(reti, &regex, errmsg, sizeof(errmsg));
        printf("Regex match failed.\n %s", errmsg);
        exit(1);

      }
      token = strtok(NULL, delim);
    }
  }
  fclose(fp5);
  regfree(&regex);
  free(line);
  free(line1);
  free(errmsg);
}


