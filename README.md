# Data loss in C

I have discovered, while coding, that one can lose data stored in C data structures. This code stores some data in a pointer and then prints it out in a function. The pointer is of-course an argument of the function. The result is 100% data loss.
This could be an internal problem of c pointers. I would much appreciate any comments and suggestions.